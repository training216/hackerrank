package main

import (
	"fmt"
	"math"
)

var (
	meal_cost float64
	tip_percent int32
	tax_percent int32
)

func main() {
	fmt.Scan(&meal_cost)
	fmt.Scan(&tip_percent)
	fmt.Scan(&tax_percent)

	tip := (float64(tip_percent)/100) * meal_cost
	tax := (float64(tax_percent)/100) * meal_cost
	fmt.Println(tip)
	fmt.Println(tax)
	totalCost := meal_cost + tip + tax
	fmt.Println(math.Round(totalCost))
}
