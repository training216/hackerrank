package main

import "fmt"

var n int
var output string

func main() {
	fmt.Scan(&n)

	if n == 1 {
		output = "Weird"
	} else if n >=2 && n <=5 {
		if n%2 == 1{
			output = "Weird"
		} else {
			output = "Not Weird"
		}
	} else if n >= 6 && n <= 20 {
		output = "Weird"
	} else if n > 20 {
		if n%2 == 1{
			output = "Weird"
		} else {
			output = "Not Weird"
		}
	}

	fmt.Println(output)
}
