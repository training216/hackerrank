//package main
//
//import (
//	"bufio"
//	"fmt"
//	"os"
//	"strconv"
//	"strings"
//)
//
//func main() {
//	var size int
//	datamap := make(map[string]int)
//	fmt.Scan(&size)
//	// untuk strings split
//	scanner := bufio.NewScanner(os.Stdin)
//	for i := 1; i <= size; i++ {
//		//var data string
//		for scanner.Scan() {
//			data := scanner.Text()
//			newData := strings.Split(data," ")
//			name := newData[0]
//			phone, _ := strconv.Atoi(newData[1])
//			datamap[name] = phone
//			break
//		}
//	}
//
//	mapToArray := []string{}
//	for key, _ := range datamap {
//		mapToArray = append(mapToArray, key)
//	}
//
//	array := make([]string, size)
//	array2 := make([]string, size)
//	for i:=0; i<len(datamap); i++ {
//		for scanner.Scan() {
//			data := scanner.Text()
//			array[i] = data
//			search := contains(mapToArray, array[i])
//			//fmt.Println("mapToArray : ", mapToArray)
//			//fmt.Println("array[i] : ", array[i])
//			//fmt.Println("search", search)
//			if search {
//				array2[i] = data
//			}
//			break
//		}
//	}
//
//	for key, value:=range(datamap) {
//		search := contains(array2, key)
//		if search {
//			fmt.Println(key + "=" + strconv.Itoa(value))
//		} else {
//			fmt.Println("Not found")
//		}
//	}
//}
//
//func contains(slice []string, item string) bool {
//	set := make(map[string]struct{}, len(slice))
//	for _, s := range slice {
//		set[s] = struct{}{}
//	}
//
//	_, ok := set[item]
//	return ok
//}

package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var mapSize int

func main() {
	fmt.Scan(&mapSize)
	phoneBook := make(map[string]int)

	scanner := bufio.NewScanner(os.Stdin)
	for i := 1; i <= mapSize; i++ {
		for scanner.Scan() {
			data := scanner.Text()
			newData := strings.Split(data, " ")
			name := newData[0]
			phone, _ := strconv.Atoi(newData[1])
			phoneBook[name] = phone
			break
		}
	}
	for scanner.Scan() {
		if scanner.Text() == "" {
			break
		}
		name :=  scanner.Text()
		_, prs := phoneBook[name]
		if prs {
			fmt.Printf("%v=%v\n", name, phoneBook[name])
		} else {
			fmt.Println("Not found")
		}
	}
}