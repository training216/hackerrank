package main

import "fmt"

func factorial(n int32) int32 {
	if n == 1 {
		return 1
	} else {
		return n * factorial(n-1)
	}
}

func main() {
	var n int32
	fmt.Scan(&n)
	result := factorial(n)
	fmt.Println(result)
}
