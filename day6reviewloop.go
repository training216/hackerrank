package main

import (
	"fmt"
)

func main() {

	var t int
	fmt.Scan(&t)

	for i:=1; i <= t; i++ {
		var str string
		var even, odd string
		fmt.Scan(&str)
		for i, k := range str{
			if i%2 == 0 {
				even += string(k)
				fmt.Println(even)
			} else if i%2 != 0 {
				odd += string(k)
			}
		}
		//fmt.Println(even, odd)
	}
}
