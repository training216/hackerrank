//package main
//
//import (
//	"bufio"
//	"fmt"
//	"os"
//	"strconv"
//	"strings"
//)
//
//func main() {
//	var arraySize int
// 	//var arrayElements string
//	fmt.Scan(&arraySize)
//	array := make([]int, arraySize)
//	arrayElements := bufio.NewScanner(os.Stdin)
//	arrayElements.Scan()
//	data := arrayElements.Text()
//	// strings.Split Hasilnya berupa slice string.
//	newData := strings.Split(data, " ")
//	size := len(array)
//
//	for _, value := range newData {
//		array[size-1], _ = strconv.Atoi(value)
//		size--
//	}
//	for _, value := range array {
//		fmt.Print(value, " ")
//	}
//}


package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)



func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16 * 1024 * 1024)

	nTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
	checkError(err)
	n := int32(nTemp)

	arrTemp := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	var arr []int32

	for i := 0; i < int(n); i++ {
		arrItemTemp, err := strconv.ParseInt(arrTemp[i], 10, 64)
		checkError(err)
		arrItem := int32(arrItemTemp)
		arr = append(arr, arrItem)
	}

	arraySize := n
	array := make([]int32, arraySize)
	// strings.Split Hasilnya berupa slice string.
	newData := arr
	size := len(array)

	for _, value := range newData {
		array[size-1] = value
		size--
	}
	for _, value := range array {
		fmt.Print(value, " ")
	}
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
