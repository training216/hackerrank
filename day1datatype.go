package main


import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var _ = strconv.Itoa // Ignore this comment. You can still use the package "strconv".

	var i uint64 = 4
	var d float64 = 4.0
	var s string = "HackerRank "

	scanner := bufio.NewScanner(os.Stdin)
	// Declare second integer, double, and String variables.
	var (
		a uint64
		b float64
		c string
	)

	// Read and save an integer, double, and String to your variables.
	fmt.Scan(&a)
	fmt.Scan(&b)
	scanner.Scan()
	c = scanner.Text()

	// Print the sum of both integer variables on a new line.
	fmt.Println(i + a)
	// Print the sum of the double variables on a new line.
	fmt.Printf("%.1f\n", d + b)
	// Concatenate and print the String variables on a new line
	fmt.Println(s + c)
	// The 's' variable above should be printed first.

}